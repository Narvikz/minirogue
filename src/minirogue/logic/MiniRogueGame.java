package minirogue.logic;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import minirogue.logic.states.IStates;

public class MiniRogueGame {
    private GameModel gameModel;

    public MiniRogueGame()
    {
        gameModel = new GameModel(Difficulty.Casual);
    }
    
    public GameModel getGameModel() { return gameModel; }
    public void setGameModel(GameModel gameModel) { this.gameModel = gameModel; }
    
    public GameData getGameData() { return gameModel.getGameData(); }
    public void setGameData(GameData gameData) { gameModel.setGameData(gameData); }
    
    public IStates getState() { return gameModel.getState(); }
    public void setState(IStates state) { gameModel.setState(state); }
    
    public void startGame()
    {
        gameModel.setState(getState().beginGame());
    }

    public void configureGame(Difficulty difficulty, int area) {
        gameModel.changeDifficulty(difficulty);
        gameModel.setArea(area);
        gameModel.setState(getState().configureGame());
    }
    
    public boolean nextArea()
    {
        int area = gameModel.getGameData().getArea();
        int food = gameModel.getGameData().getFood();
        int health = gameModel.getGameData().getHealth();
        
        if (area < 14)
        {
            if (food >= 1)
                gameModel.getGameData().setFood(food - 1);
            else
                gameModel.getGameData().setHealth(health - 2);
            
            gameModel.getGameData().setArea(area + 1);
            
            return true;
        }
        else
            return false;
    }
    
    public boolean nextTurn()
    {
        int column = gameModel.getGameData().getColumn();
        
        if (column < 5)
        {
            gameModel.getGameData().setColumn(column + 1);
            
            return true;
        }
        else if (column == 5)
            return nextArea();
        
        return false;
    }

    public void generateLevel() {
        gameModel.getGameData().generateLevel();
    }
}
