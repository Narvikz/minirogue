package minirogue.logic;

public enum Difficulty {
    Casual,
    Normal,
    Hard,
    Impossible
}
