package minirogue.logic;

import java.io.Serializable;

public class Card implements Serializable
{
    private String name;

    public Card() { }

    public Card(String name)
    {
        this.name = name;
    }

    @Override
    public String toString() { 
        return this.name;
    }    
}
