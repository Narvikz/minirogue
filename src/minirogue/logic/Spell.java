package minirogue.logic;

import java.io.Serializable;

public abstract class Spell implements Serializable
{
    private String name;

    public Spell() { }

    public Spell(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
