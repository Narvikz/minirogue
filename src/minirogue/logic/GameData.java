package minirogue.logic;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class GameData implements Constants, Serializable
{
    private int area;
    private int column;
    private int health;
    private int gold;
    private int food;
    private int armor;
    private int experience;
    private ArrayList<Spell> spells;
    private ArrayList<Card> cards;

    public GameData(Difficulty d)
    {
        switch (d)
        {
            case Casual:
                this.health = 5;
                this.gold = 5;
                this.food = 6;
                this.armor = 1;
                break;
            case Normal:
                this.health = 5;
                this.gold = 3;
                this.food = 6;
                this.armor = 0;
                break;
            case Hard:
                this.health = 4;
                this.gold = 2;
                this.food = 5;
                this.armor = 0;
                break;
            case Impossible:
                this.health = 3;
                this.gold = 1;
                this.food = 3;
                this.armor = 0;
                break;
        }
        
        this.area = 1;
        this.column = 1;
        this.experience = 0;
        this.spells = new ArrayList<>();
        this.cards = new ArrayList<>();
    }

    public GameData getGameData() { return this; }
    
    public void changeDifficulty(Difficulty d)
    {
        switch (d)
        {
            case Casual:
                this.health = 5;
                this.gold = 5;
                this.food = 6;
                this.armor = 1;
                break;
            case Normal:
                this.health = 5;
                this.gold = 3;
                this.food = 6;
                this.armor = 0;
                break;
            case Hard:
                this.health = 4;
                this.gold = 2;
                this.food = 5;
                this.armor = 0;
                break;
            case Impossible:
                this.health = 3;
                this.gold = 1;
                this.food = 3;
                this.armor = 0;
                break;
        }
    }

    public int getArea() { return this.area; }
    public void setArea(int area) { this.area = area; }
    public int getColumn() { return this.column; }
    public void setColumn(int column) { this.column = column; }
    public int getFood() { return this.food; }
    public void setFood(int food) { this.food = food; }
    public int getHealth() { return this.health; }
    public void setHealth(int health) { this.health = health; }
    public ArrayList<Card> getCards() { return this.cards; }
    
    public void generateLevel()
    {
        ArrayList<Card> cards = new ArrayList<>();    
        
        cards.add(new Card("Monster card"));
        cards.add(new Card("Treasure card"));
        cards.add(new Card("Merchant card"));
        cards.add(new Card("Resting card"));
        cards.add(new Card("Event card"));
        cards.add(new Card("Trap card"));
        
        Collections.shuffle(cards);
        
        if (isBossLevel())
            cards.add(new Card("Boss Monster card"));
    }
    
    public int getLevel()
    {
        if (this.area >= 1 && this.area <= 2) return 1;
        else if (this.area >= 3 && this.area <= 4) return 2;
        else if (this.area >= 5 && this.area <= 7) return 3;
        else if (this.area >= 8 && this.area <= 10) return 4;
        else if (this.area >= 11 && this.area <= 14) return 5;
        
        return 0;
    }
    
    public boolean isBossLevel()
    {
        return this.area == 2 || this.area == 4 || this.area == 7 || this.area == 10 || this.area == 14;
    }
}
