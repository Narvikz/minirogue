package minirogue.logic.states;

import minirogue.logic.GameData;

public class StateAdapter implements IStates
{
    protected GameData gameData;

    public StateAdapter(GameData game)
    {
        this.gameData = game;
    }

    public GameData getGameData()
    {
        return gameData;
    }
    
    public void setGameData(GameData game)
    {
        this.gameData = game;
    }
    
    @Override
    public IStates beginGame()
    {
        return new AwaitCardSelection(this.gameData);
    }

    @Override
    public IStates configureGame() {
        return this;
    }

    @Override
    public IStates restingCardSelected()
    {
        return new AwaitRestingCardOption(this.gameData);
    }
    
    @Override
    public IStates treasureEventCardSelected()
    {
        return new AwaitRestingCardOption(this.gameData);
    }

    @Override
    public IStates merchantCardSelected() {
        return this;
    }

    @Override
    public IStates monsterCardSelected() {
        return this;
    }

    @Override
    public IStates updateResources() {
        return this;
    }

    @Override
    public IStates rollDice() {
        return this;
    }

    @Override
    public IStates applyDamageOnMonster() {
        return this;
    }

    @Override
    public IStates chooseToFeat() {
        return this;
    }

    @Override
    public IStates performFeatOnDie() {
        return this;
    }

    @Override
    public IStates bothAlive() {
        return this;
    }

    @Override
    public IStates monsterDies() {
        return new AwaitCardSelection(this.gameData);
    }

    @Override
    public IStates finalBossDies() {
        return new VictoryScreen(this.gameData);
    }

    @Override
    public IStates nextTurn() {
       return new AwaitCardSelection(this.gameData);
    }

    @Override
    public IStates backToStart() {
        return new AwaitBeginning(this.gameData);
    }
}
