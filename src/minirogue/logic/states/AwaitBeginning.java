package minirogue.logic.states;

import minirogue.logic.GameData;

public class AwaitBeginning extends StateAdapter
{
    public AwaitBeginning(GameData g) { super(g); }

    @Override
    public IStates configureGame() {
        return this;
    }
}
