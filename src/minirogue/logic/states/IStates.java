package minirogue.logic.states;

import java.io.Serializable;

public interface IStates extends Serializable
{
    //AwaitBeginning
    IStates configureGame();
    IStates beginGame();
    
    //AwaitCardSelection
    IStates restingCardSelected();
    IStates treasureEventCardSelected();
    IStates merchantCardSelected();
    IStates monsterCardSelected();
    
    //AwaitTrading
    IStates updateResources();
    //nextRound
    
    //Combat
    IStates rollDice();
    IStates applyDamageOnMonster();
    IStates chooseToFeat();
    
    //Feat
    IStates performFeatOnDie();
    //applyDamage
    
    //Spell
    IStates bothAlive();
    IStates monsterDies();
    IStates finalBossDies();
    
    //VictoryScreen - BONUS STATE
    IStates backToStart();
    
    //
    IStates nextTurn();
}