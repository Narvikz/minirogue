package minirogue.logic;

import java.io.Serializable;
import minirogue.logic.states.*;
import minirogue.logic.Difficulty;

public class GameModel implements Serializable
{
    private GameData gameData;
    private IStates state;
    
    public GameModel(Difficulty d)
    {
        gameData = new GameData(d);
        setState(new AwaitBeginning(gameData));
    }
    
    public GameData getGameData() { return gameData; }
    public void setGameData(GameData gameData) { this.gameData = gameData; }
    public IStates getState() { return state; }
    public void setState(IStates state) { this.state = state; }
    public void changeDifficulty(Difficulty d) { this.gameData.changeDifficulty(d); }
    public void setArea(int a) { this.gameData.setArea(a); }
}