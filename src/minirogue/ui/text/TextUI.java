package minirogue.ui.text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import minirogue.logic.*;

public class TextUI {
    private MiniRogueGame game;
    private boolean quit = false;

    public TextUI(MiniRogueGame game)
    {
        this.game = game;
    }
    
    public void cleanScreen()
    {
        for (int i = 0; i < 30; System.out.println(), i++);
    }
    
    public void uiAwaitBeginning()
    {
        Scanner sc = new Scanner(System.in);
        BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
        String option, fileName;
        char c = 0;
        
        while (true)
        {
            do {
                cleanScreen();
                System.out.println("MINI ROGUE");
                System.out.println();
                System.out.println("0 - Quit");
                System.out.println("1 - Start");
                System.out.println("2 - Continue from a previously saved state");
                System.out.println();
                System.out.print("> ");

                option = sc.next();

                if(option.length() >= 1)
                    c = option.charAt(0);
                else 
                    continue;

            } while(c < '0' || c > '2');

            switch(c)
            {
                case '0':
                    quit = true;

                    return;
                case '1':
                    Difficulty difficulty = Difficulty.Casual;
                    int area = 0;
                    
                    do
                    {
                        cleanScreen();
                        System.out.println("Select Difficulty");
                        System.out.println();
                        System.out.println("0 - Casual");
                        System.out.println("1 - Normal");
                        System.out.println("2 - Hard");
                        System.out.println("3 - Impossible");
                        System.out.println();
                        System.out.print("> ");

                        option = sc.next();
                        if(option.length() >= 1)
                            c = option.charAt(0);
                        else 
                            continue;

                    } while(c < '0' || c > '3');
                    
                    difficulty = Difficulty.values()[Character.getNumericValue(c)];
                    
                    do
                    {
                        cleanScreen();
                        System.out.println("Select area to start in");
                        System.out.println();
                        System.out.println("Level 1 - Area 1 to 2");
                        System.out.println("Level 2 - Area 3 to 4");
                        System.out.println("Level 3 - Area 5 to 7");
                        System.out.println("Level 4 - Area 8 to 10");
                        System.out.println("Level 5 - Area 11 to 14");
                        System.out.println();
                        System.out.print("> ");

                        option = sc.next();
                        
                        area = Integer.parseInt(option.trim());

                    } while(area < 1 || area > 14);
                    
                    game.configureGame(difficulty, area);
                    game.generateLevel();
                    
                    cleanScreen();
                    System.out.println("Game started!");
                    game.startGame();

                    return;
                case '2':
                    //TODO
                    System.out.print("File name: ");

                    try
                    {
                        fileName = bin.readLine();
                        if(fileName == null || fileName.length() < 1) return;
                        //game.setGameModel((GameModel)FileUtility.retrieveGameFromFile(new File(fileName)));
                        //game.nextRound();   // Mudar o estado
                    }
                    catch(Exception e) { return; }

                    return;
                default:
                    return;
            }
        }
    }
    
    public void uiAwaitCardSelection()
    {
        ArrayList<Card> cards = game.getGameData().getCards();
        int level = game.getGameData().getLevel();
        int column = game.getGameData().getColumn();
        
        cleanScreen();
        System.out.println("You are on level " + level + " on column " + column + ". These are the cards you are facing:");
        System.out.println();
        
        if (column == 1)
        {
            System.out.println("1 - " + cards[0]);
        }
        else if (column == 2)
        {
            System.out.println("1 - " + cards[1]);
            System.out.println("2 - " + cards[2]);
        }
        else if (column == 3)
        {
            System.out.println("1 - " + cards[3]);
        }
        else if (column == 4)
        {
            System.out.println("1 - " + cards[4]);
            System.out.println("2 - " + cards[5]);
        }
        else if (column == 5)
        {
            System.out.println("1 - " + cards[6]);
        }
        
        System.out.println();
        System.out.print("> ");
    }

    public void run() {
        while (!quit)
        {
            IStates state = game.getState();
            
            if (state instanceof AwaitBeginning)
            {
                uiAwaitBeginning();
            }
            else if (state instanceof AwaitCardSelection)
            {
                uiAwaitCardSelection();
            }
        }
    }
}
