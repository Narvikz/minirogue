package minirogue;

import minirogue.ui.text.TextUI;
import minirogue.logic.MiniRogueGame;

public class MiniRogueTextUI
{
    public static void main(String[] args)
    {
        TextUI textUI = new TextUI(new MiniRogueGame());
        textUI.run();
    }
}